<?php

declare(strict_types=1);

namespace App\Service;

class ManageUrlService
{
    public const BASE_URL = 'https://swapi.py4e.com/api/';

    public function getUrlWithCategory(string $category, ?int $id = null): string
    {
        return $id ? self::BASE_URL.$category.'/'.$id : self::BASE_URL.$category.'/';
    }

    public function getIdByApiUrl(string $url, array $items): array
    {
        $datas = [];
        foreach ($items as $key => $item) {
            $datas[$key] = $item;
            $datas[$key]['id'] = (int) str_replace('/', '', str_replace($url, '', $item['url']));
        }

        return $datas;
    }
}
