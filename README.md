# May The Force Be

A Star Wars website with [Symfony 7](https://symfony.com), [Docker](https://www.docker.com/), [Php 8.2](https://www.php.net/releases/8.2/en.php/), [Bootstrap 4](https://getbootstrap.com/) and [Caddy 2](https://caddyserver.com/).

This Symfony application run with [Star Wars API](https://swapi.py4e.com/api/)

So, ready Padawan ???

## Getting Started

Clone the project

```bash
  git clone https://gitlab.com/froldev/que-la-force-soit.git maytheforcebe
```

Create .env.local

## Local environment

For a local environment, add in the .env.local file

```bash
APP_ENV=dev
```

## Prod environment

For online, add to the .env.local file

```bash
APP_ENV=prod
```

## Installation

Run the docker-compose

```bash
  docker compose up -d --build
```

Log into the PHP container

```bash
  docker exec -it maytheforcebe_php bash
```

Install your Symfony application

```bash
  make install
  exit
```

*Your application is available at http://127.0.0.1:8746*

## Ready to use with

This docker-compose provides you :

- PHP-8.0.13-cli (Debian)
    - Composer
    - Symfony CLI
    - and some other php extentions
    - nodejs, npm, yarn
- caddy 2

## Requirements

Out of the box, this docker-compose is designed for a Linux operating system, provide adaptations for a Mac or Windows environment.

- Docker
- Docker compose

## Author

Froldev
- [Website](https://fredolive.fr)
- [Gitlab](https://gitlab.com/froldev)
- [LinkedIn](https://www.linkedin.com/in/fred-olive/)