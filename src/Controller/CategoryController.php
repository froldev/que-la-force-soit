<?php

namespace App\Controller;

use App\Service\ManageUrlService;
use App\Service\StarWarsApiService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    private const CATEGORIES = [
        [
            'name' => 'films',
            'category' => 'films',
            'lightsaber' => 'sentinel',
            'image' => 'img/films-filtre.jpeg',
        ],
        [
            'name' => 'personnages',
            'category' => 'people',
            'lightsaber' => 'jedi',
            'image' => 'img/people-filtre.jpeg',
        ],
        [
            'name' => 'planètes',
            'category' => 'planets',
            'lightsaber' => 'consular',
            'image' => 'img/planets-filtre.jpeg',
        ],
        [
            'name' => 'espèces',
            'category' => 'species',
            'lightsaber' => 'guardian',
            'image' => 'img/species-filtre.jpeg',
        ],
        [
            'name' => 'vaisseaux',
            'category' => 'starships',
            'lightsaber' => 'fashionista',
            'image' => 'img/starships-filtre.jpeg',
        ],
        [
            'name' => 'véhicules',
            'category' => 'vehicles',
            'lightsaber' => 'sith',
            'image' => 'img/vehicles-filtre.jpeg',
        ],
    ];

    private const NUMBER_ITEMS = 9;

    public function __construct(
        private readonly StarWarsApiService $starWarsApiService,
        private readonly ManageUrlService $manageUrlService,
        private readonly RequestStack $requestStack,
        private readonly PaginatorInterface $paginator,
    ) {
    }

    #[Route('/{category}/{id}', name: 'app_category_detail', requirements: ['id' => '\d+'])]
    public function detail(string $category, int $id): Response
    {
        $url = $this->manageUrlService->getUrlWithCategory($category, $id);

        return $this->render('category/detail.html.twig', [
            'item' => $this->starWarsApiService->getItem($url),
            'category' => $category,
        ]);
    }

    #[Route('/{category}', name: 'app_category')]
    public function index(string $category): Response
    {
        $request = $this->requestStack->getMainRequest();

        $url = $this->manageUrlService->getUrlWithCategory($category);

        $items = $this->starWarsApiService->getCollection($url);

        $items = $this->paginator->paginate(
            $this->manageUrlService->getIdByApiUrl($url, $items),
            $request->query->getInt('page', 1),
            self::NUMBER_ITEMS,
        );

        return $this->render('category/index.html.twig', [
            'items' => $items,
            'dataCategory' => self::CATEGORIES[array_search($category, array_column(self::CATEGORIES, 'category'))],
        ]);
    }
}
