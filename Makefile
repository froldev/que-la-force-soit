#---Symfony-And-Docker-Makefile---------------#
# License: MIT
#---------------------------------------------#

#---VARIABLES---------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_COMPOSE = docker compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
#------------#

#---SYMFONY--#
SYMFONY = symfony
SYMFONY_SERVER_START = $(SYMFONY) serve -d
SYMFONY_SERVER_STOP = $(SYMFONY) server:stop
SYMFONY_CONSOLE = $(SYMFONY) console
SYMFONY_LINT = $(SYMFONY_CONSOLE) lint:
#------------#

#---COMPOSER-#
COMPOSER = composer
COMPOSER_INSTALL = $(COMPOSER) install
COMPOSER_UPDATE = $(COMPOSER) update
COMPOSER_DUMP_DEV = $(COMPOSER) dump-env dev
COMPOSER_DUMP_PROD = $(COMPOSER) dump-env prod
#------------#

#---YARN-----#
YARN = yarn
YARN_INSTALL = $(YARN) install --force
YARN_UPDATE = $(YARN) update
YARN_BUILD = $(YARN) run build
YARN_DEV = $(YARN) run dev
YARN_WATCH = $(YARN) run watch
#------------#

#---PHPQA---#
PHPQA = jakzal/phpqa:php8.2
PHPQA_RUN = $(DOCKER_RUN) --init --rm -v $(PWD):/project -w /project $(PHPQA)
#------------#

#---PHPUNIT-#
PHPUNIT = APP_ENV=test $(SYMFONY) php bin/phpunit
#------------#
#---------------------------------------------#

## === 🆘  HELP ==================================================
help: ## Show this help.
	@echo "Symfony-And-Docker-Makefile"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#---------------------------------------------#

## === 🐋  DOCKER ================================================
docker-up: ## Start docker containers.
	$(DOCKER_COMPOSE_UP)

docker-stop: ## Stop docker containers.
	$(DOCKER_COMPOSE_STOP)
#---------------------------------------------#

## === 🎛️  SYMFONY ===============================================
sf: ## List and Use All Symfony commands (make sf command="commande-name").
	$(SYMFONY_CONSOLE) $(command)

sf-start: ## Start symfony server.
	$(SYMFONY_SERVER_START)

sf-stop: ## Stop symfony server.
	$(SYMFONY_SERVER_STOP)

sf-cc: ## Clear symfony cache.
	$(SYMFONY_CONSOLE) cache:clear

sf-log: ## Show symfony logs.
	$(SYMFONY) server:log

sf-dc: ## Create symfony database.
	$(SYMFONY_CONSOLE) doctrine:database:create --if-not-exists

sf-dd: ## Drop symfony database.
	$(SYMFONY_CONSOLE) doctrine:database:drop --if-exists --force

sf-su: ## Update symfony schema database.
	$(SYMFONY_CONSOLE) doctrine:schema:update --force

sf-mm: ## Make migrations.
	$(SYMFONY_CONSOLE) make:migration

sf-dmm: ## Migrate.
	$(SYMFONY_CONSOLE) doctrine:migrations:migrate --no-interaction

sf-fixtures: ## Load fixtures.
	$(SYMFONY_CONSOLE) doctrine:fixtures:load --no-interaction

sf-me: ## Make symfony entity
	$(SYMFONY_CONSOLE) make:entity

sf-mc: ## Make symfony controller
	$(SYMFONY_CONSOLE) make:controller

sf-mf: ## Make symfony Form
	$(SYMFONY_CONSOLE) make:form

sf-perm-var: ## Fix permissions.
	chmod -R 777 var

sf-perm-uploads: ## Fix permissions.
	chmod -R 777 public/uploads

sf-dump-env: ## Dump env.
	$(SYMFONY_CONSOLE) debug:dotenv

sf-dump-env-container: ## Dump Env container.
	$(SYMFONY_CONSOLE) debug:container --env-vars

sf-dump-routes: ## Dump routes.
	$(SYMFONY_CONSOLE) debug:router

sf-open: ## Open project in a browser.
	$(SYMFONY) open:local

sf-open-email: ## Open Email catcher.
	$(SYMFONY) open:local:webmail

sf-check-requirements: ## Check requirements.
	$(SYMFONY) check:requirements
#---------------------------------------------#

## === 🐋  COMMAND ================================================

sf-command: ## Command to install the project.
	php bin/console app:install
#---------------------------------------------#

## === 📦  COMPOSER ==============================================
composer-install: ## Install composer dependencies.
	$(COMPOSER_INSTALL)

composer-update: ## Update composer dependencies.
	$(COMPOSER_UPDATE)

composer-dump-dev: ## Dump dev env.
	$(COMPOSER_DUMP_DEV)

composer-dump-prod: ## Dump prod env.
	$(COMPOSER_DUMP_PROD)

composer-validate: ## Validate composer.json file.
	$(COMPOSER) validate

composer-validate-deep: ## Validate composer.json and composer.lock files in strict mode.
	$(COMPOSER) validate --strict --check-lock
#---------------------------------------------#

## === 📦  YARN ===================================================
yarn-install: ## Install npm dependencies.
	$(YARN_INSTALL)

yarn-update: ## Update npm dependencies.
	$(YARN_UPDATE)

yarn-build: ## Build assets.
	$(YARN_BUILD)

yarn-dev: ## Build assets in dev mode.
	$(YARN_DEV)

yarn-watch: ## Watch assets.
	$(YARN_WATCH)
#---------------------------------------------#

## === 🐛  PHPQA =================================================
qa-cs-fixer-dry-run: ## Run php-cs-fixer in dry-run mode.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose --dry-run

qa-cs-fixer: ## Run php-cs-fixer.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose

qa-phpstan: ## Run phpstan.
	$(PHPQA_RUN) phpstan analyse ./src --level=3

qa-security-checker: ## Run security-checker.
	$(SYMFONY) security:check

qa-phpcpd: ## Run phpcpd (copy/paste detector).
	$(PHPQA_RUN) phpcpd ./src

qa-php-metrics: ## Run php-metrics.
	$(PHPQA_RUN) phpmetrics --report-html=var/phpmetrics ./src

qa-lint-twigs: ## Lint twig files.
	$(SYMFONY_LINT)twig ./templates

qa-lint-yaml: ## Lint yaml files.
	$(SYMFONY_LINT)yaml ./config

qa-lint-container: ## Lint container.
	$(SYMFONY_LINT)container

qa-lint-schema: ## Lint Doctrine schema.
	$(SYMFONY_CONSOLE) doctrine:schema:validate --skip-sync -vvv --no-interaction

qa-audit: ## Run composer audit.
	$(COMPOSER) audit
#---------------------------------------------#

## === 🔎  TESTS =================================================
tests: ## Run tests.
	$(PHPUNIT) --testdox

tests-coverage: ## Run tests with coverage.
	XDEBUG_MODE=coverage $(PHPUNIT) --coverage-html var/coverage
#---------------------------------------------#

## === ⭐  OTHERS =================================================
before-commit: qa-cs-fixer qa-phpstan qa-security-checker qa-phpcpd qa-lint-twigs qa-lint-yaml qa-lint-container qa-lint-schema tests ## Run before commit.

install: composer-install yarn-install sf-perm-var sf-cc ## Install.

reset-db: ## Reset database.
	$(eval CONFIRM := $(shell read -p "Are you sure you want to reset the database? [y/N] " CONFIRM && echo $${CONFIRM:-N}))
	@if [ "$(CONFIRM)" = "y" ]; then \
		$(MAKE) sf-dd; \
		$(MAKE) sf-dc; \
		$(MAKE) sf-dmm; \
	fi
.PHONY: reset-db
#---------------------------------------------#
