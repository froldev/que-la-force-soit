<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

enum Type: string
{
    case Item = 'ITEM';
    case Collection = 'COLLECTION';
}

class StarWarsApiService
{
    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly ManageUrlService $ManageUrlService,
        private CacheInterface $cache,
    ) {
    }

    public function getCollection(string $url): array
    {
        $key = explode('/', $url);
        $items = $this->cache->get($key[4], function () use ($url) {
            return $this->makeRequest(Type::Collection->name, $url);
        });

        return $items;
    }

    public function getItem(string $url): array
    {
        $key = explode('/', $url);
        $item = $this->cache->get($key[4].'_'.$key[5], function () use ($url) {
            return $this->makeRequest(Type::Item->name, $url);
        });

        return $item;
    }

    private function makeRequest(string $type, string $url): array
    {
        $response = $this->httpClient->request('GET', $url);

        $data = match ($type) {
            Type::Item->name => $response->toArray(),
            Type::Collection->name => $this->getAllItems($url),
        };

        return $data;
    }

    private function getAllItems(string $url): array
    {
        $response = $this->httpClient->request('GET', $url);

        $data = $response->toArray()['results'];

        while ($response->toArray()['next']) {
            $response = $this->httpClient->request('GET', $response->toArray()['next']);
            $data = array_merge($data, $response->toArray()['results']);
        }

        return $data;
    }
}
